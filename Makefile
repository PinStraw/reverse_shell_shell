LIBRARIES = -Iinclude
OUTPUT_DIR = ./bin
OUTPUT_BIN = ${OUTPUT_DIR}/PROG
OUTPUT = -o ${OUTPUT_BIN}
SOURCES = ./src/* ./cmd/shell.c

build: output_dir
	gcc -Wall -std=gnu99 ${LIBRARIES} ${SOURCES} ${OUTPUT:PROG=rshell}

debug: output_dir
	gcc -Wall -g ${LIBRARIES} ${SOURCES} ${OUTPUT:PROG=rshell}

output_dir:
	mkdir -p ${OUTPUT_DIR}

install: build
	mv ${OUTPUT_BIN:PROG=rshell} /usr/sbin/rshell
