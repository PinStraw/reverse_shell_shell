#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include "cmd_utils.h"

#define clear() printf("\033[H\033[J")

static char *builtin_functions[] = {
        "tixe",
        "pleh",
        "rohtua"
};

int (*builtin_func[]) (char **) = {
        &reverse_exit,
        &reverse_help,
        &show_author
};

int builtin_func_num() {
        return sizeof(builtin_functions) / sizeof(char *);
}

int show_author() {
        static const char* author_message =
                "Author: Spencer\n"
                "Description: My first \"reverse shell\"\n"
                "(╯°□°）╯︵ ┻━┻\n"
                "";

        printf("%s", author_message);

        return 0;
}

void init_shell() {
        static const char* init_message =
                "======================================\n"
                "= This Shell is *NOT* for Production =\n"
                "=   For more information run help.   =\n"
                "=   For information on the author:   =\n"
                "=             run author.            =\n"
		"=              READ ME!!             =\n"
                "======================================\n";

        clear();
        printf("%s", init_message);

        return;
}

int reverse_exit(char **args) {
        return 255;
}

int reverse_help(char **args) {
        printf("ESREVER\n");
        return 0;
}

char *read_input() {
        int c, input_len = 0;
        char *user_input = calloc(0, (input_len+1) * sizeof(char));

        printf("$ ");

        while((c=getchar()) !='\n' && c != EOF) {
                input_len++;
                user_input = realloc(user_input, (input_len+1)*sizeof(char));
                user_input[input_len-1] = (char)c;
                user_input[input_len] = '\0';
        }

        if(input_len == 0) {
                user_input=NULL;
                return user_input;
        }

        return user_input;
}

int reverse_external_execute(char **args) {
        pid_t pid;
        int status;

        pid = fork();
        if (pid == 0) {
                if(execvp(args[0], args) == -1) {
                        printf("ERROR\n");
			//return 1;
                }
                exit(EXIT_FAILURE);
        } else if(pid < 0) {
                printf("PID ERROR\n");
		return 1;
        } else {
                do {
                        waitpid(pid, &status, WUNTRACED);
                } while(!WIFEXITED(status) && !WIFSIGNALED(status));
        }

        return status;
}

void reverse_command(char *args) {
        char *temp_string;
        temp_string = calloc(0, strlen(args));
        strcpy(temp_string, args);
        for(int i = 0; i < strlen(args); i++) {
                temp_string[i] = args[strlen(args)-(i+1)];
        }
        strcpy(args, temp_string);
        free(temp_string);
        return;
}

int reverse_execute(char **args) {
        if (args[0] == NULL) {
                return -1;
        }

        for(int i = 0; i < builtin_func_num(); i++) {
                if(strcmp(args[0], builtin_functions[i]) == 0) {
                        return (*builtin_func[i])(args);
                }
        }
        return reverse_external_execute(args);
}

char **split_cmd(char *line) {
        size_t size = 8;
        int pos = 0;
        char **args = calloc(0, size * sizeof(char*));
        char *arg = NULL;

        if(line == NULL) {
                *args=NULL;
                return args;
        }

        reverse_command(line);

        arg = strtok(line, " \t\r\n\a");
        while (arg != NULL) {
                args[pos] = arg;
                pos++;

                if (pos >= size) {
                        size += 1;
                        args = realloc(args, size * sizeof(char*));

                }
                arg = strtok(NULL, " \t\r\n\a");
        }

        args[pos] = NULL;
        return args;
}
