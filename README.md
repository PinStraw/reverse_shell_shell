# Reverse Shell Shell

**IMPORTANT** this shell is **NOT** meant to be used for any serious applications.
This is for academic purposes only!

## Install

```
make build
make install
```
