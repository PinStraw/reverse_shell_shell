#include <stdio.h>
#include <stdlib.h>
#include "cmd_utils.h"

#define clear() printf("\033[H\033[J")

int main() {
	int status_return = 0;
	char *cmd;
	char **args;
	int numFail = 0;
	int numSuccess = 0;

	init_shell();

	do {
		cmd = read_input();
		args = split_cmd(cmd);
		status_return = reverse_execute(args);

		free(cmd);
		free(args);

		// Check exit status for feedback
		if ( status_return > 0 ) {
			numFail++;
			// Check for fail help messages
			if ( numFail == 2 ) {
				printf("Have you read the output of the `author` command?\n");
			} else if ( numFail == 10 ) {
				printf("Maybe we try typing backwards?\n");
			}
		} else if ( status_return == 0 ) {
			numSuccess++;
			// Check for success help messages
			if ( numSuccess == 4 ) {
				printf("Might be worth trying to figure out why this is your shell?\n");
			} else if ( numSuccess == 10 ) {
				printf("Random trivia: Shells are defined in /etc/passwd\n");
			}
		} // else command was NULL and skip
	} while(status_return != 255);
	return EXIT_SUCCESS;
}
