#include <stdio.h>
#include <stdlib.h>

int builtin_func_num();
int show_author();
void init_shell();
int reverse_exit(char **args);
int reverse_help(char **args);
char *read_input();
int reverse_external_execute(char **args);
void reverse_command(char *args);
int reverse_execute(char **args);
char **split_cmd(char *line);

